### Requirements
- ansible
- virtualbox
- vagrant
- yq
- kubectl


### Set up host environment
```bash
echo '* 0.0.0.0/0 ::/0' | sudo tee -a /etc/vbox/networks.conf
ansible-galaxy install -r ansible/requirements.yaml
```

### Set up virtual env
```bash
# ssh jan@bt -L 9900:172.16.16.100:22 -L 9901:172.16.16.101:22 -L 9911:172.16.16.111:22
vagrant up --provider=virtualbox && vagrant snapshot save vagrant-up
cd ansible
ansible-playbook infra.yaml && vagrant snapshot save ansible
cd ..
```

### Run in kmaster
```bash
#vagrant ssh kmaster
sudo su - kube
sudo kubeadm config images pull
sudo kubeadm init --apiserver-advertise-address=172.16.16.100 --apiserver-cert-extra-sans=bt,10.38.6.86 --pod-network-cidr=192.168.0.0/16 | tee -a ~/kubeinit.log
sudo kubeadm token create --print-join-command | tee ~/joincluster.sh
ln -s /etc/kubernetes/admin.conf ~/.kube/config
exit
sudo apt install acl && sudo setfacl -m u:kube:r /etc/kubernetes/admin.conf
```

### On host
```bash
./scripts/joinClusterWithNodes.sh
./scripts/locaKubectl.sh
kubectl config use-context bt_trojaj12
```

### Test
```bash
kubectl get nodes
```

### Calico
```bash
kubectl create -f https://docs.projectcalico.org/v3.18/manifests/calico.yaml
```

see [manifest repo](https://gitlab.com/school_trojaj12/bachelor_thesis/manifests)

---

### BT
Aby fungoval ARP, tak se musi pridat `net.ipv4.conf.all.proxy_arp=1` flag pro kernel na edge edgenodexh
```bash
ansible-playbook bt.yaml && vagrant snapshot save bt
```